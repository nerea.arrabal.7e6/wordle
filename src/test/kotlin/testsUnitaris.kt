import org.junit.jupiter.api.Test
import java.io.File
import java.util.*
import kotlin.test.assertEquals
import kotlin.test.assertTrue

class WordleTest {

    // Per a la funció "readWordsFromFile"
    @Test
    fun testReadWordsFromFile() {
        val filePath = "src/test/kotlin/testWords.txt"
        val file = File(filePath)
        file.writeText("hola adeu\n")
        val expectedWords = listOf("hola", "adeu")
        assertEquals(expectedWords, readWordsFromFile(filePath))
        file.delete()
    }

    // Per a la funció "chooseRandomWord"
    @Test
    fun testChooseRandomWord() {
        val words = listOf("hello", "world", "foo", "bar", "baz")
        val randomWord = chooseRandomWord(words)
        assertTrue { randomWord in words }
    }

    // Per a la funció "playWordle"
    @Test
    fun testPlayWordle() {
        val maxAttempts = 6
        val scanner = Scanner("abcde\nfghij\nklmno\npqrst\nuvwxy\nzzzzz\n")
        val language = "EN"
        val chosenWord = "hello"
        val (wordFound, attempts) = playWordle(chosenWord, maxAttempts, scanner, language)
        assertTrue { !wordFound }
        assertEquals(maxAttempts, attempts)
    }

    // Per a la funció "checkWord"
    @Test
    fun testCheckWord() {
        val guess = "hello"
        val chosenWord = "world"
        val (correctPositions, correctLetters) = checkWord(guess, chosenWord)
        assertEquals(1, correctPositions)
        assertEquals(3, correctLetters)
    }

    // Per a la funció "printColoredWordStatus"
    @Test
    fun testPrintColoredWordStatus() {
        val guess = "world"
        val chosenWord = "hello"
        printColoredWordStatus(guess, chosenWord)
    }
}