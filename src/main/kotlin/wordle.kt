/**
 * @author Nerea Arrabal
 * @version 0.2
 */

import java.util.*
import java.io.File
/**
Diccionari que conté els fitxers amb les paraules disponibles en cada idioma.
 */
private val WORD_FILES = mapOf<String, String>(
    "ES" to "src/main/kotlin/wordsEsp.txt",
    "EN" to "src/main/kotlin/wordsEng.txt"
)

fun main() {
    val scanner = Scanner(System.`in`)
    val maxAttempts = 6
    var language = "EN"

    val gamesHistory = mutableListOf<Pair<String, Boolean>>()

    var exit = false
    while (!exit) {
        println("\n----- Wordle Menu -----")
        println("1. Change language (current language: $language)")
        println("2. View games history")
        println("3. Play Wordle")
        println("4. Exit")
        println("----------------------")

        print("Enter your choice (1-4): ")
        val choice = scanner.nextInt()

        when (choice) {
            1 -> {
                language = chooseLanguage(scanner)
                println("Language changed to $language")
            }
            2 -> {
                printGamesHistory(gamesHistory)
            }
            3 -> {
                val words = readWordsFromFile(WORD_FILES.get(language)!!)
                println("Starting a new game in $language...")
                val chosenWord = chooseRandomWord(words)
                println(chosenWord)
                scanner.nextLine() // Consumir la línea vacía
                val (wordFound, attempts) = playWordle(chosenWord, maxAttempts, scanner, language)
                gamesHistory.add(Pair(chosenWord, wordFound))
                println("The word was $chosenWord. You ${if (wordFound) "win" else "lose"} in $attempts attempts.")
                var playAgain = true
                playAgain = playAgain(scanner, language)
                if (playAgain) {
                    playWordle(chosenWord, maxAttempts, scanner, language)
                }
            }
            4 -> {
                exit = true
                println("Goodbye!")
            }
            else -> {
                println("Invalid choice.")
            }
        }
    }
}

/**
Llegeix les paraules del fitxer i les retorna en una llista.
@param filePath La ruta del fitxer que conté les paraules.
@return Una llista amb les paraules del fitxer.
 */
fun readWordsFromFile(filePath: String): List<String> {
    var file = File(filePath)
    var words = mutableListOf<String>()
    file.forEachLine { line ->
        val lineWords = line.split(" ")
        words.addAll(lineWords)
    }
    return words
}


/**
Escull una paraula aleatòria de la llista de paraules.
@param words Una llista amb les paraules disponibles.
@return La paraula escollida.
 */
fun chooseRandomWord(words: List<String>): String {
    if (words.isEmpty()) {
        return ""
    }
    return words.random()
}


/**
 * Juga a Wordle.
 *
 * @param chosenWord la paraula que s'ha d'endevinar
 * @param maxAttempts el número màxim d'intents per endevinar la paraula
 * @param scanner l'objecte Scanner per llegir l'entrada de l'usuari
 * @param language el codi d'idioma (actualment no s'utilitza)
 * @return una parella amb el booleà que indica si s'ha endevinat la paraula i el número d'intents
 */
fun playWordle(chosenWord: String, maxAttempts: Int, scanner: Scanner, language: String): Pair<Boolean, Int> {
    var attempts = 0
    var wordFound = false
    var playAgain = true

    while (playAgain && !wordFound && attempts < maxAttempts) {
        println("Please enter a five-letter word:")
        val guess = scanner.nextLine().uppercase()
        attempts++

        if (guess.length != 5) {
            println("Your word should be five letters long.")

        } else {
            val (correctPositions, correctLetters) = checkWord(guess, chosenWord)
            printColoredWordStatus(guess, chosenWord)

            if (correctPositions == 5) {
                wordFound = true
            }
        }

        if (!wordFound && attempts == maxAttempts) {
            println("You lose. The word was $chosenWord.")
            playAgain = playAgain(scanner, language)
            if (playAgain) {
                playWordle(chosenWord, maxAttempts, scanner, language)
            }
        }
    }

    return Pair(wordFound, attempts)
}

/**

Revisa la posició de les lletres del jugador en vers a la paraula del joc.

@param guess La paraula introduïda pel jugador.

@param chosenWord La paraula del joc.

@return Un Pair amb el número de lletres correctes a la posició correcta i el número de lletres correctes a la posició incorrecta.
 */

fun checkWord(guess: String, chosenWord: String): Pair<Int, Int> {
    var correctPositions = 0
    var correctLetters = 0

    for (i in guess.indices) {
        if (i < chosenWord.length && guess[i] == chosenWord[i]) {
            correctPositions++
        } else if (chosenWord.contains(guess[i])) {
            correctLetters++
        }
    }

    return Pair(correctPositions, correctLetters)
}

/**

Retorna la paraula introduïda per el jugador amb els colors corresponents per cada lletra.

@param guess La paraula introduïda pel jugador.

@param chosenWord La paraula del joc.
 */

fun printColoredWordStatus(guess: String, chosenWord: String) {
    //ANSI colors

    val gray = "\u001b[90;1m"

    val yellow = "\u001b[33;1m"

    val green = "\u001b[32;1m"

    // Resets previous color codes

    val reset = "\u001b[0m"

    var result = ""

    for((index, c) in guess.withIndex()) {

        if(index < chosenWord.length){

            if(chosenWord.contains(c) && (guess.indexOf(c, index) == chosenWord.indexOf(c, index))){
                result = result + green + c + reset
            }

            else if(chosenWord.contains(c) && (guess.indexOf(c, index) != chosenWord.indexOf(c, index))){
                result = result + yellow + c + reset
            }

            else{
                result = result + gray + c + reset
            }
        }
    }
    //End loop and print result
    println(result)
}

/**

Submenú per canviar d'idioma.
@param scanner L'objecte Scanner per llegir l'entrada de l'usuari.
@return El nou idioma seleccionat.
 */

fun chooseLanguage(scanner: Scanner): String {

    while (true) {
        println("Please choose a language / Elige un idioma")
        println("1. English")
        println("2. Spanish")

        when (scanner.nextInt()) {
            1 -> return "EN"
            2 -> return "ES"
            else -> println("Invalid choice. Please enter a number between 1 and 3.")
        }
    }
}


/**
 * Pregunta si l'usuari vol jugar de nou un cop que ha guanyat o perdut el joc.
 *
 * @param scanner Scanner per llegir l'entrada de l'usuari.
 * @param language llengua en què es mostrarà la pregunta.
 * @return Retorna `true` si l'usuari vol jugar de nou, `false` si no.
 */

fun playAgain(scanner: Scanner, language: String): Boolean {
    while (true) {
        println("Do you want to play again? (y/n)")
        val answer = scanner.nextLine()
        when (answer.uppercase()) {
            "Y", "YES" -> return true
            "N", "NO" -> return false
            else -> {
                println("Please enter a valid answer.")
            }
        }
    }
}

/**
 * Imprimeix l'historial dels jocs jugats.
 *
 * @param gamesHistory Llista que conté parells de paraules i booleans que representen si l'usuari va guanyar o perdre el joc.
 */

fun printGamesHistory(gamesHistory: List<Pair<String, Boolean>>) {
    if (gamesHistory.isEmpty()) {
        println("No games played yet.")
    } else {
        println("Games History:")
        var totalAttempts = 0
        var totalWins = 0
        var longestWinStreak = 0
        var currentWinStreak = 0
        for ((word, won) in gamesHistory) {
            totalAttempts++
            if (won) {
                totalWins++
                currentWinStreak++
                if (currentWinStreak > longestWinStreak) {
                    longestWinStreak = currentWinStreak
                }
            } else {
                currentWinStreak = 0
            }
            println("- $word: ${if (won) "WIN" else "LOSE"}")
        }
        val winPercentage = totalWins.toDouble() / totalAttempts * 100
        val averageAttempts = totalAttempts.toDouble() / gamesHistory.size
        println("Total games played: ${gamesHistory.size}")
        println("Total wins: $totalWins")
        println("Win percentage: ${"%.2f".format(winPercentage)}%")
        println("Average attempts: ${"%.2f".format(averageAttempts)}")
        println("Longest win streak: $longestWinStreak")
    }
}