# WORDLE

## INTRODUCCIÓ

Wordle és un joc de paraules en el que has d'endevinar una paraula de cinc lletres en un número limitat d'intents.

El programa conté un diccionari amb paraules disponibles en diferents idiomes i ofereix la possibilitat de jugar en l'idioma que triïs. També guarda un historial dels jocs jugats.

## REQUISITS

Per a poder executar el programa és necessari tenir instal·lat el JDK de Java en la seva versió 8 o superior.

## EXECUCIÓ

Per a executar el programa, segueix les següents instruccions:

    1. Descarrega el fitxer wordle.kt.
    2. Obre un terminal i navega fins al directori on estigui el fitxer descarregat.
    3. Executa la següent comanda per a compilar el programa:

```
kotlinc -include-runtime wordle.kt -d wordle.jar

```

    1.Executa la següent comanda per a executar el programa:

```
java -jar wordle.ja

```

Al llançar el joc, apareixerà un menú amb les següents opcions:

    1. Canviar idioma
    2. Veure història de jocs
    3. Jugar a Wordle
    4. Sortir

# Canviar idioma

En seleccionar aquesta opció, se li demanarà que introdueixi el codi d'idioma (ES per espanyol, EN per anglès).

# Veure història de jocs

Aquesta opció mostra una llista de totes les paraules que s'han jugat i si l'usuari les ha endevinat o no.

# Jugar a Wordle

Aquesta opció inicia un nou joc, es mostrarà un missatge que indica que s'està iniciant un nou joc en l'idioma actual. A continuació, es seleccionarà una paraula de la llista de paraules disponibles en l'idioma actual, i es donarà a l'usuari un límit d'intents per endevinar la paraula, després de cada intent, es mostrarà una resposta que indicarà quantes lletres estan en la posició correcta i quantes lletres són correctes però estan en la posició incorrecta. El joc finalitzarà quan l'usuari endevini la paraula o excedeixi el límit d'intents.

# Sortir

Aquesta opció finalitza el programa.


## NORMES DEL JOC

- L'objectiu és endevinar una paraula secreta de 5 lletres.
- En cada partida es disposa de 6 intents per endevinar la paraula.
- Si la lletra està situada en el lloc adequat, el color serà verd.
- Si la lletra està dins de la paraula, però no ben situada, el color serà groc
- Si no és cap de les dues opcions anteriors, la lletra s'acoloreix de grisa.
- Les paraules que genera el joc no contemplen lletres com la ç, ñ ni accents o caràcters estranys.

## IMPLEMENTACIÓ

Aquest codi implementa el joc Wordle en Kotlin. El programa permet als usuaris canviar d'idioma, veure l'historial dels jocs anteriors, jugar Wordle i sortir del programa.

La funció `readWordsFromFile` llegeix les paraules del fitxer que rep com a paràmetre i les retorna en una llista. La funció `chooseRandomWord` escull una paraula aleatòria de la llista de paraules. La funció `playWordle` és la funció principal que executa el joc. L'usuari té un màxim de 6 intents per endevinar la paraula. La funció `checkWord` comprova la posició de les lletres del jugador respecte a la paraula del joc, i la funci `printColoredWordStatus` retorna la paraula introduïda pel jugador amb els colors corresponents per cada lletra.